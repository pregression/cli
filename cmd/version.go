/*
Copyright © 2019 Pregression, Inc

Licensed under the Apache License, version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var version = "undefined"

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Display the current version",
	Long:  `Get the current version of the Pregression CLI`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(versionStr())
	},
}

func versionStr() string {
	return version
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
