package cmd

import (
	"testing"
)

func TestVersionCommand(t *testing.T) {
	version = "0.1.0"

	if versionStr() != version {
		t.Errorf("Expected '%s' but received '%s'", version, versionStr())
	}
}
