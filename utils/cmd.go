// Most of these funcs come from
// @see https://chromium.googlesource.com/external/github.com/spf13/cobra/+/refs/heads/master/command_test.go
package utils

import (
	"bytes"

	"github.com/spf13/cobra"
)

func EmptyRun(*cobra.Command, []string) {}

func ExecuteCommand(root *cobra.Command, args ...string) (output string, err error) {
	_, output, err = executeCommandC(root, args...)
	return output, err
}

func executeCommandC(root *cobra.Command, args ...string) (c *cobra.Command, output string, err error) {
	buf := new(bytes.Buffer)
	root.SetOutput(buf)
	root.SetArgs(args)

	c, err = root.ExecuteC()

	return c, buf.String(), err
}
