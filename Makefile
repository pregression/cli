BINARY=pregression
VERSION=`git describe --tags`
BUILD_PATH=./build

LDFLAGS=-ldflags "-X gitlab.com/pregression/cli/cmd.version=${VERSION} -extldflags '-static'"

build: build-darwin32 build-darwin64 build-linux32 build-linux64 build-windows32 build-windows64

build-darwin32:
	GOOS=darwin GOARCH=386 go build ${LDFLAGS} -o ${BUILD_PATH}/darwin/386/${BINARY}

build-darwin64:
	GOOS=darwin GOARCH=amd64 go build ${LDFLAGS} -o ${BUILD_PATH}/darwin/amd64/${BINARY}

build-linux32:
	GOOS=linux GOARCH=386 go build ${LDFLAGS} -o ${BUILD_PATH}/linux/386/${BINARY}

build-linux64:
	GOOS=linux GOARCH=amd64 go build ${LDFLAGS} -o ${BUILD_PATH}/linux/amd64/${BINARY}

build-windows32:
	GOOS=windows GOARCH=386 go build ${LDFLAGS} -o ${BUILD_PATH}/windows/386/${BINARY}.exe

build-windows64:
	GOOS=windows GOARCH=amd64 go build ${LDFLAGS} -o ${BUILD_PATH}/windows/amd64/${BINARY}.exe

clean:
	if [ -d ${BUILD_PATH} ] ; then rm -r ${BUILD_PATH}; fi

.PHONY: clean
